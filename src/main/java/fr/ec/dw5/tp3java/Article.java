package fr.ec.dw5.tp3java;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @NotNull
    @Column(name = "titre")
    private String titre;
    @NotNull
    @Column(name = "contenu")
    private String contenu;
    @ManyToOne
    @JoinColumn(name = "auteur")
    private Auteur auteur;



    public Article() {
    }

    public Article(@NotNull String titre, @NotNull String contenu) {
        this.titre = titre;
        this.contenu = contenu;

    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Auteur getAuteur() {
        return auteur;
    }

    public void setAuteur(Auteur auteur) {
        this.auteur = auteur;
    }
}
