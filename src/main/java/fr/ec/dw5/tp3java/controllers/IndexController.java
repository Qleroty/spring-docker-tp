package fr.ec.dw5.tp3java.controllers;

import fr.ec.dw5.tp3java.Article;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class IndexController {

    @RequestMapping("/")
    public String index(Model model) {
        Article article = new Article();
        article.setTitre("Il se brosse les dents : Ca tourne mal !");
        article.setContenu("Ceci est le contenu");

        model.addAttribute("article", article);
       return "index";
    }
}
