package fr.ec.dw5.tp3java.controllers;

import fr.ec.dw5.tp3java.Article;
import fr.ec.dw5.tp3java.Auteur;
import fr.ec.dw5.tp3java.interfaces.ArticleRepository;
import fr.ec.dw5.tp3java.interfaces.AuteurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/new_author")
public class AuteurController {

    @Autowired
    private AuteurRepository auteurRepository;

    @GetMapping()
    public String formAuteur(Model model) {
        model.addAttribute("auteur", new Auteur());
        return "formAuteur";
    }
    @PostMapping("")
    public String traitementForm(@Valid @ModelAttribute Auteur auteur, Errors errors) {
        if (errors.hasErrors()) {
            return "formAuteur";
        } else {
            auteurRepository.save(auteur);

            System.out.println("Titre de l'article : "+auteur.getNom());
            return "redirect:/";
        }
    }
}
