package fr.ec.dw5.tp3java.controllers;

import fr.ec.dw5.tp3java.Article;
import fr.ec.dw5.tp3java.Auteur;
import fr.ec.dw5.tp3java.interfaces.ArticleRepository;
import fr.ec.dw5.tp3java.interfaces.AuteurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/new")
public class FormController {

    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private AuteurRepository auteurRepository;

    @GetMapping()
    public String form(Model model) {
        model.addAttribute("article", new Article());

        List<Auteur> listeAuteurs = auteurRepository.findAll();


        model.addAttribute("auteurs", listeAuteurs);



        return "form";
    }
    @PostMapping("")
    public String traitementForm(@Valid @ModelAttribute Article article, Errors errors) {
        if (errors.hasErrors()) {
            return "form";
        } else {
            articleRepository.save(article);

            System.out.println("Titre de l'article : "+article.getTitre());
            return "redirect:/";
        }
    }
}
