package fr.ec.dw5.tp3java.interfaces;

import fr.ec.dw5.tp3java.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article, String> {

    Article save(Article article);
}
