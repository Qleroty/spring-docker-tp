package fr.ec.dw5.tp3java.interfaces;

import fr.ec.dw5.tp3java.Auteur;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AuteurRepository extends JpaRepository<Auteur, String> {

}

