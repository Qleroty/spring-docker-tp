package fr.ec.dw5.tp3java;

import fr.ec.dw5.tp3java.interfaces.ArticleRepository;
import fr.ec.dw5.tp3java.interfaces.AuteurRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tp3JavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tp3JavaApplication.class, args);
	}

	public void test(ArticleRepository articleRepository, AuteurRepository auteurRepository) {
	}
}
