package fr.ec.dw5.tp3java;

import fr.ec.dw5.tp3java.interfaces.AuteurRepository;

import javax.persistence.*;
import java.util.List;

@Entity
public class Auteur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "nom")
    private String nom;


    public Auteur() {
    }

    public Auteur(String nom) {
        this.nom = nom;

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Long getId() {
        return id;
    }

}
