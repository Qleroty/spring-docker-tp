CREATE TABLE article (
    id SERIAL NOT NULL PRIMARY KEY,
    titre TEXT NOT NULL,
    contenu TEXT NOT NULL,
    auteur INT REFERENCES auteur
);
