FROM maven:3-jdk-8-slim
MAINTAINER Quentin Leroty
COPY . /source
RUN cd /source && mvn clean package -Dmaven.test.skip=true


ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/source/target/tp3-java-0.0.1-SNAPSHOT.jar"]